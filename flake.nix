{
  description = "Description for the project";

  inputs = {
    flake-parts.url = "github:hercules-ci/flake-parts";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    systems.url = "github:nix-systems/default";
  };
  outputs = inputs @ {flake-parts, ...}:
    flake-parts.lib.mkFlake {inherit inputs;} {
      systems = import inputs.systems;
      perSystem = {
        config,
        self',
        inputs',
        pkgs,
        system,
        ...
      }: {
        devShells.default = pkgs.mkShell {
          packages = with pkgs; [
            geckodriver
            nodePackages.pyright
            ruff
            (python3.withPackages (python-pkgs: [
              python-pkgs.selenium
            ]))
          ];
          shellHook = with pkgs; ''
            export WEBDRIVER_GECKO_DRIVER=${lib.getExe geckodriver}
            export SE_AVOID_BROWSER_DOWNLOAD=true
            export SE_AVOID_STATS=true
          '';
        };
        formatter = pkgs.alejandra;
      };
    };
}
