import argparse
import csv
import os
import pathlib

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import (
    NoSuchElementException,
    ElementNotInteractableException,
)

URL: str = "https://forms.office.com/Pages/ResponsePage.aspx?id=lICmegRhpkG0Q9S1JFH2F4vqiVYSvL5PliJB2hV6h4VUMEw5MFI0WURSVzBPODNJNE1IM1BLWkxNVS4u"

LABEL_QUESTIONID_MAPPING = {
    "name": "re62298b77cc146118d23373fbe3434ba",
    "lucat": "rbaa66270b5ea43deb7447a4cdb24d9d9",
    "agreement_bool": "r358df58fd5ae4f4abd380cc1da41c873",
    "reading_comprehension": "r143100dd3e5e49b781c552949fda8bee",
    "intro_bool": "r3c230ce3549c4593a9908dace14bbc19",
    "intro_where": "ra340fe95afd848af9c37d5ddc47be322",
}

SUBMIT_BTN_IDENTIFIER = ("data-automation-id", "submitButton")


def fill_input(driver, label: str, value: str | bool):
    wait = WebDriverWait(
        driver,
        timeout=2,
        poll_frequency=0.2,
        ignored_exceptions=[NoSuchElementException, ElementNotInteractableException],
    )
    question_id = LABEL_QUESTIONID_MAPPING[label]
    if isinstance(value, bool):
        match value:
            case True:
                value_attr = "Yes"
            case False:
                value_attr = "No"

        xpath = f"//input[(@name='{question_id}') and (@value='{value_attr}')]"
        element = wait.until(lambda x: x.find_element(By.XPATH, xpath))
        element.click()

    else:
        xpath = f"//input[contains(@aria-labelledby, '{question_id}')]"
        element = wait.until(lambda x: x.find_element(By.XPATH, xpath))
        element.send_keys(value)


def fill_form_from_csv(csv_file: os.PathLike) -> None:
    driver = webdriver.Firefox()

    with pathlib.Path(csv_file).open(mode="r") as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            driver.get(URL)

            print(row)
            fill_input(driver, "name", row["name"])

            lucat_maybe_w_domain = row["lucat"]
            lucat = lucat_maybe_w_domain.replace("@lu.se", "")

            fill_input(driver, "lucat", lucat)

            fill_input(driver, "agreement_bool", True)

            fill_input(driver, "reading_comprehension", "bot@entry.ai")

            fill_input(driver, "intro_bool", True)
            fill_input(driver, "intro_where", "bot entry")

            attr_name, attr_value = SUBMIT_BTN_IDENTIFIER
            driver.find_element(
                By.XPATH, f"//button[@{attr_name}='{attr_value}']"
            ).click()

    # Close the browser
    driver.quit()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Fill form from CSV")
    parser.add_argument("csv_file", help="Path to the CSV file", type=pathlib.Path)
    args = parser.parse_args()

    fill_form_from_csv(args.csv_file)
