.PHONY: all watch clean

TEXFILES := $(wildcard *.tex)
PDFFILES := $(TEXFILES:.tex=.pdf)

LATEXPROG := latexmk
LATEXFLAGS := -pdf -xelatex -interaction=nonstopmode

all: $(PDFFILES)

%.pdf: %.tex
	$(LATEXPROG) $(LATEXFLAGS) $<

watch:
	$(LATEXPROG) $(LATEXFLAGS) -pvc

clean:
	$(LATEXPROG) -C
